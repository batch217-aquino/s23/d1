let myTrainer = {
	name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends:{
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },

	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(myTrainer);

console.log("Result of dot notation:");
console.log(myTrainer.name);

console.log("Result of square bracket notation:");
console.log(myTrainer['pokemon']);

let trainer = {
    pokemon: "Pikachu",
	talk: function(){
        console.log("Result of talk method");
		console.log(this.pokemon+"! I choose you!");
	}
}



trainer.talk();


function Pokemon(name, level){
	
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		target.health = target.health - this.attack;
		console.log(this.name + " tackled " + target.name);
		console.log( target.name + "'s health is now reduced to " + target.health);

		if(target.health <= 0){
			this.faint(target);
		}
	}

	this.faint = function(target){
		console.log( target.name + " fainted");
	}
}

let pokemon1 = new Pokemon("Pikachu", 12);
let pokemon2 = new Pokemon("Geodude", 8);
let pokemon3 = new Pokemon("Mewtwo", 100);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon2.tackle(pokemon1);
console.log(pokemon1);

pokemon3.tackle(pokemon2);
console.log(pokemon2);
